local config = {
	updater = {
		remote = "origin",
		channel = "nightly",
		version = "latest",
		-- branch = "v4",
		commit = nil,
		pin_plugins = nil,
		skip_prompts = false,
		show_changelog = true,
		auto_quit = false,
	},
	-- colorscheme = "oxocarbon",
	highlights = {},
	options = {},
	diagnostics = {
		virtual_text = true,
		underline = true,
	},

	lazy = {
		defaults = { lazy = true },
		performance = {
			rtp = {
				disabled_plugins = { "tohtml", "gzip", "matchit", "zipPlugin", "netrwPlugin", "tarPlugin" },
			},
		},
	},
	lsp = {
		servers = {
			"dartls",
		},
		formatting = {
			format_on_save = {
				enabled = true,
				allow_filetypes = {},
				ignore_filetypes = {},
			},
			disabled = {},
			timeout_ms = 1000,
		},
		mappings = {
			n = {},
		},
		config = {},
	},
	-- cmp = {
	-- 	source_priority = {
	-- 		copilot = 621,
	-- 		nvim_lua = 1000,
	-- 		nvim_lsp = 700,
	-- 		luasnip = 650,
	-- 		crates = 600,
	-- 		path = 500,
	-- 		buffer = 250,
	-- 	},
	-- },
	heirline = {},
	polish = function()
		vim.keymap.set("n", "<C-s>", ":w!<CR>")

		local augend = require("dial.augend")
		require("dial.config").augends:register_group({
			default = {
				augend.integer.alias.decimal,
				augend.integer.alias.hex,
				augend.date.alias["%Y/%m/%d"],
			},
			typescript = {
				augend.integer.alias.decimal,
				augend.integer.alias.hex,
				augend.constant.alias.bool,
				augend.constant.alias.bool,
				augend.date.alias["%Y/%m/%d"],
				augend.constant.new({
					elements = { "&&", "||" },
					word = false,
					cyclic = true,
				}),
				augend.constant.new({
					elements = { "!=", "==" },
					word = false,
					cyclic = true,
				}),
				augend.constant.new({ elements = { "let", "const" } }),
			},
			visual = {
				augend.integer.alias.decimal,
				augend.integer.alias.hex,
				augend.date.alias["%Y/%m/%d"],
				augend.constant.alias.alpha,
				augend.constant.alias.Alpha,
			},
		})

		local map = vim.api.nvim_set_keymap

		map("n", "<C-a>", require("dial.map").inc_normal(), { noremap = true })
		map("n", "<C-x>", require("dial.map").dec_normal(), { noremap = true })
		map("n", "<leader>re", "<cmd>LspRestart<CR>", { noremap = true })

		-- YANKY
		map("n", "p", "<Plug>(YankyPutAfter)", {})
		map("n", "P", "<Plug>(YankyPutBefore)", {})
		map("x", "p", "<Plug>(YankyPutAfter)", {})
		map("x", "P", "<Plug>(YankyPutBefore)", {})
		map("n", "gp", "<Plug>(YankyGPutAfter)", {})
		map("n", "gP", "<Plug>(YankyGPutBefore)", {})
		map("x", "gp", "<Plug>(YankyGPutAfter)", {})
		map("x", "gP", "<Plug>(YankyGPutBefore)", {})

		-- Diagnostic
		map("n", "ge", "<cmd>lua vim.diagnostic.open_float()<CR>", {})

		map("n", "<c-n>", "<Plug>(YankyCycleForward)", {})
		map("n", "<c-p>", "<Plug>(YankyCycleBackward)", {})

		map("n", "y", "<Plug>(YankyYank)", {})
		map("x", "y", "<Plug>(YankyYank)", {})

		-- Spectre

		map("n", "<leader>rr", "<cmd>lua require('spectre').open()<CR>", {})
		map("n", "<leader>rc", "<cmd>lua require('spectre').open_visual({select_word=true})<CR>", {})
		map("n", "<leader>rf", "<cmd>lua require('spectre').open_file_search()<CR>", {})
		map("n", "<leader>rs", "<cmd>lua require('spectre').open_file_search({select_word=true})<CR>", {})

		local Terminal = require("toggleterm.terminal").Terminal
		local pter = Terminal:new({ cmd = "pter ~/todo.txt", hidden = true, direction = "tab" })

		---@diagnostic disable-next-line: lowercase-global
		function _lazygit_toggle()
			local lazygit = Terminal:new({ cmd = "lazygit", hidden = false, direction = "tab" })
			lazygit:toggle()
		end

		---@diagnostic disable-next-line: lowercase-global
		function _pter_toggle()
			pter:toggle()
		end

		map("n", "<leader>ao", "<cmd>TSLspOrganize<cr>", { noremap = true, desc = "Organize Imports", silent = true })
		map("n", "<leader>aa", "<cmd>TSLspImportAll<cr>", { noremap = true, desc = "Import All", silent = true })
		map("n", "<leader>ar", "<cmd>TSLspRenameFile<cr>", { noremap = true, desc = "Rename File", silent = true })

		vim.keymap.set("n", "<leader>se", function()
			require("scissors").editSnippet()
		end)

		-- When used in visual mode prefills the selection as body.
		vim.keymap.set({ "n", "x" }, "<leader>sa", function()
			require("scissors").addNewSnippet()
		end)

		-- astronvim.add_cmp_source("copilot", 700)

		-- astronvim.add_user_cmp_source({
		-- 	name = "copilot",

		-- require("catppuccin").setup()
		-- require("telescope").load_extension("gh")
		-- local dap = require("dap")
		-- dap.adapters.codelldb = {
		-- 	type = "server",
		-- 	port = "${port}",
		-- 	executable = {
		-- 		command = "/usr/bin/codelldb",
		-- 		args = { "--port", "${port}" },
		-- 	},
		-- }
		-- dap.configurations.rust = {
		-- 	{
		-- 		name = "Rust debug",
		-- 		type = "codelldb",
		-- 		request = "launch",
		-- 		program = function()
		-- 			return vim.fn.input("Path to executable: ", vim.fn.getcwd() .. "/target/debug/", "file")
		-- 		end,
		-- 		cwd = "${workspaceFolder}",
		-- 		stopOnEntry = true,
		-- 	},
		-- }
	end,
}

function hasQFOpened()
	local buffers = vim.api.nvim_list_bufs()
	local res = false

	for _, buffer in ipairs(buffers) do
		if vim.api.nvim_buf_is_loaded(buffer) then
			local filename = vim.api.nvim_buf_get_option(buffer, "filetype")

			if filename == "qf" and vim.fn.getbufinfo(buffer)[1].hidden == 0 then
				res = true
			end
		end
	end

	return res
end

return config
