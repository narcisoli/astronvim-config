return {

	n = {
		["<leader>st"] = {
			":Telescope live_grep<cr>",
			desc = "Search word",
		},
		["<leader>sc"] = {
			":Telescope grep_string<cr>",
			desc = "Search word",
		},

		-- ["<leader>sr"] = { ":Telescope current_buffer_fuzzy_find<cr>" },
		["<leader>sr"] = {
			function()
				require("telescope").extensions.recent_files.pick()
			end,
			desc = "Search recent files",
		},

		["<leader>lt"] = { ":Telescope current_buffer_fuzzy_find<cr>" },
		["<leader>sp"] = { ":SessionManager load_session<cr>" },

		["<leader>ss"] = {
			":SessionManager save_current_session<cr>",
			desc = "Fuzzy find in file",
		},
		["<leader>lw"] = {
			":Telescope diagnostics<cr>",
			desc = "Diagnostics",
		},
		["<leader>go"] = {
			":Telescope git_status<cr>",
			desc = "Git files",
		},
		["<leader>gb"] = {
			":Telescope git_bcommits<cr>",
			desc = "BCommits",
		},
		["<leader>sf"] = { ":Telescope find_files<cr>" },
		["s"] = { ":HopWord<cr>" },
		["S"] = { ":HopWord<cr>" },
		["<leader>lS"] = { ":Telescope lsp_workspace_symbols<cr>" },
		["<leader>lR"] = { ":LspRestart<cr>" },
		["<leader>gg"] = { "<cmd>lua _lazygit_toggle()<CR>" },
		["<leader>gt"] = { "<cmd>lua _pter_toggle()<CR>" },
		["<leader>Q"] = { "<cmd>qa!<CR>" },
		-- ["<leader>q"] = { "<cmd>q!<CR>" },
		["<leader>W"] = { "<cmd>wa<CR>" },
		["<leader>lj"] = { "<cmd>lua vim.diagnostic.goto_next()<cr>" },
		["<leader>lk"] = { "<cmd>lua vim.diagnostic.goto_prev()<cr>" },
		["<TAB>"] = {
			function()
				if hasQFOpened() then
					pcall(vim.cmd, "cnext")
				else
					require("harpoon.ui").nav_next()
				end
			end,
			desc = "do something",
		},
		["<S-TAB>"] = {
			function()
				if hasQFOpened() then
					pcall(vim.cmd, "cprevious")
				else
					require("harpoon.ui").nav_prev()
				end
			end,
			desc = "do something",
		},
		["<leader>h"] = {
			function()
				require("harpoon.ui").toggle_quick_menu()
			end,
			desc = "Toggle harpoon menu",
		},
		["<leader>H"] = {
			function()
				require("harpoon.mark").add_file()
			end,
			desc = "Toggle harpoon menu",
		},
	},
	x = {

		["<leader>sa"] = {
			function()
				require("scissors").addNewSnippet()
			end,
			desc = "Add snippet",
		},
	},
	t = {},
}
