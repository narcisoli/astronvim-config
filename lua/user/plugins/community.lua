return {
	"AstroNvim/astrocommunity",
	{ import = "astrocommunity.pack.svelte" },
	{ import = "astrocommunity.pack.tailwindcss" },
	{ import = "astrocommunity.pack.typescript" },
	{ import = "astrocommunity.pack.rust" },
	-- { import = "astrocommunity.pack.php" },
	{ import = "astrocommunity.pack.lua" },
	{ import = "astrocommunity.pack.terraform" },
	{ import = "astrocommunity.pack.json" },
	{ import = "astrocommunity.pack.docker" },
	{ import = "astrocommunity.pack.yaml" },
	-- { import = "astrocommunity.note-taking.obsidian-nvim" },
	{ import = "astrocommunity.lsp.garbage-day-nvim" },
	-- { import = "astrocommunity.pack.docker" },
	-- { import = "astrocommunity.scrolling.cinnamon-nvim" },
	-- { import = "astrocommunity.completion.copilot-lua-cmp" },

	-- { import = "astrocommunity.colorscheme.nightfox",  enabled = false },
	-- { import = "astrocommunity.colorscheme.mellow-nvim" },
	-- { import = "astrocommunity.colorscheme.oxocarbon-nvim" },
	-- { import = "astrocommunity.debugging.nvim-bqf", enabled = false },
	-- { import = "astrocommunity.colorscheme.rose-pine" },
	-- { import = "astrocommunity.colorscheme.catppuccin" },
	-- { import = "astrocommunity.completion.copilot-lua" },
	-- {
	--   -- further customize the options set by the community
	--   "copilot.lua",
	--   opts = {
	--     suggestion = {
	--       keymap = {
	--         accept = "<C-l>",
	--         accept_word = false,
	--         accept_line = false,
	--         next = "<C-.>",
	--         prev = "<C-,>",
	--         dismiss = "<C/>",
	--       },
	--     },
	--   },
	-- },
	-- { import = "astrocommunity.bars-and-lines.smartcolumn-nvim" },
	-- {
	--   "m4xshen/smartcolumn.nvim",
	--   opts = {
	--     colorcolumn = 120,
	--     disabled_filetypes = { "help" },
	--   },
	-- },
}
