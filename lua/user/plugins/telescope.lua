return {

	{
		"smartpde/telescope-recent-files",
		lazy = false,
		config = function()
			require("telescope").load_extension("recent_files")
		end,
	},
	{
		"nvim-telescope/telescope-github.nvim",
	},
	{
		"nvim-telescope/telescope.nvim",
		opts = {
			file_ignore_patterns = { "./node_modules/*", "node_modules", "^node_modules/*", "node_modules/*" },
			pickers = {
				current_buffer_fuzzy_find = {
					tiebreak = function(current_entry, existing_entry)
						return current_entry.lnum < existing_entry.lnum
					end,
				},
			},
			extensions = {
				recent_files = {
					only_cwd = true,
				},
			},
		},
	},
}
