return {
	{
		"ThePrimeagen/harpoon",
		lazy = false,
		config = function()
			require("harpoon").setup({
				menu = {
					width = 1000,
				},
			})
		end,
	},
}
