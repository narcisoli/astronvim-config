return {
	{
		"axelvc/template-string.nvim",
		lazy = false,
		config = function()
			require("template-string").setup({
				filetypes = {
					"typescript",
					"graphql",
					"svelte",
					"gql",
					"javascript",
					"typescriptreact",
					"javascriptreact",
				},
				jsx_brackets = true,
			})
		end,
	},
}
