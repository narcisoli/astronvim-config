return {

	-- {
	-- 	"anuvyklack/windows.nvim",
	-- 	requires = {
	-- 		"anuvyklack/middleclass",
	-- 		"anuvyklack/animation.nvim",
	-- 	},
	-- 	config = function()
	-- 		vim.o.winwidth = 10
	-- 		vim.o.winminwidth = 10
	-- 		vim.o.equalalways = false
	-- 		require("windows").setup()
	-- 	end,
	-- },

	-- {
	-- 	"jose-elias-alvarez/typescript.nvim",
	-- 	lazy = false,
	-- 	config = function()
	-- 		require("typescript").setup({
	-- 			disable_commands = false, -- prevent the plugin from creating Vim commands
	-- 			debug = false, -- enable debug logging for commands
	-- 		})
	-- 	end,
	-- },
	-- { "shaunsingh/oxocarbon.nvim", run = "./install.sh" },
	{
		"gbprod/yanky.nvim",
		event = "InsertEnter",
		config = function()
			require("yanky").setup({})
		end,
	},

	{
		"monaqa/dial.nvim",
	},

	{
		"wellle/targets.vim",
		lazy = false,
	},
	{ "tpope/vim-repeat" },
	{
		"kylechui/nvim-surround",
		version = "*", -- Use for stability; omit to use `main` branch for the latest features
		event = "VeryLazy",
		config = function()
			require("nvim-surround").setup({
				-- Configuration here, or leave empty to use defaults
			})
		end,
	},
	-- {
	-- 	"tpope/vim-surround",
	-- 	lazy = false,
	-- 	keys = { "c", "d", "y" },
	-- },
	-- {
	-- 	"welle/targets.vim",
	-- 	lazy = false,
	-- },
	{
		"phaazon/hop.nvim",
		event = "BufRead",
		config = function()
			require("hop").setup()
		end,
	},
}
