return {
	{
		"L3MON4D3/LuaSnip",
		config = function()
			local vsCode = require("luasnip/loaders/from_vscode")
			vsCode.load({ paths = { "~/.config/astronvim/lua/user/snippets" } })
		end,
	},
}
