local config = {
	updater = {
		remote = "origin", -- remote to use
		channel = "nightly", -- "stable" or "nightly"
		version = "latest", -- "latest", tag name, or regex search like "v1.*" to only do updates before v2 (STABLE ONLY)
		branch = "v3", -- branch name (NIGHTLY ONLY)
		commit = nil, -- commit hash (NIGHTLY ONLY)
		pin_plugins = nil, -- nil, true, false (nil will pin plugins on stable only)
		skip_prompts = false, -- skip prompts about breaking changes
		show_changelog = true, -- show the changelog after performing an update
	},
	colorscheme = "noctis",

	options = {
		opt = {
			relativenumber = false, -- sets vim.opt.relativenumber
			ignorecase = true,
			-- foldmethod = "expr",
			-- foldexpr = "nvim_treesitter#foldexpr()",
		},
		g = {
			mapleader = " ", -- sets vim.g.mapleader
		},
	},

	mappings = {
		-- first key is the mode
		n = {
			-- second key is the lefthand side of the map
			-- mappings seen under group name "Buffer"
			["<leader>sf"] = {
				function()
					require("telescope.builtin").find_files()
				end,
				desc = "Search files",
			},

			["<TAB>"] = {
				function()
					if hasQFOpened() then
						pcall(vim.cmd, "cnext")
					else
						require("harpoon.ui").nav_next() -- navigates to next mark
					end
				end,
				desc = "do something",
			},

			["<S-TAB>"] = {
				function()
					if hasQFOpened() then
						pcall(vim.cmd, "cprevious")
					else
						require("harpoon.ui").nav_prev() -- navigates to next mark
					end
				end,
				desc = "do something",
			},

			["<leader>h"] = {
				function()
					require("harpoon.ui").toggle_quick_menu()
				end,
				desc = "Toggle harpoon menu",
			},

			-- ["<leader>e"] = { "<cmd>Neotree toggle buffers<cr>", desc = "Toggle Explorer" },
			-- ["<leader>E"] = { "<cmd>Neotree toggle <cr>", desc = "Toggle Explorer" },
			-- ["<leader>o"] = { "<cmd>Neotree focus<cr>", desc = "Focus Explorer" },

			["<leader>st"] = {
				function()
					require("telescope.builtin").live_grep()
				end,
				desc = "Search word",
			},
			["<leader>ss"] = {
				function()
					require("telescope.builtin").buffers()
				end,
				desc = "Search word",
			},
			["<leader>sc"] = {
				function()
					require("telescope.builtin").grep_string()
				end,
				desc = "Search word",
			},
			["<leader>sr"] = {
				function()
					require("telescope").extensions.recent_files.pick()
				end,
				desc = "Search recent files",
			},
			["<leader>lt"] = {
				function()
					require("telescope.builtin").current_buffer_fuzzy_find()
				end,
				desc = "Fuzzy find in file",
			},
			["<leader>lw"] = {
				function()
					require("telescope.builtin").diagnostics()
				end,
				desc = "Diagnostics",
			},

			["<leader>go"] = {
				function()
					require("telescope.builtin").git_status()
				end,
				desc = "Git files",
			},
			["<leader>gb"] = {
				function()
					require("telescope.builtin").git_bcommits()
				end,
				desc = "Commits",
			},

			["<leader>lS"] = {
				function()
					require("telescope.builtin").lsp_document_symbols()
				end,
				desc = "Commits",
			},

			--
			--
			-- quick save
			-- ["<C-s>"] = { ":w!<cr>", desc = "Save File" },  -- change description but the same command
		},
		t = {
			-- setting a mapping to false will disable it
			-- ["<esc>"] = false,
		},
	},
	-- Default theme configuration
	default_theme = {
		diagnostics_style = { italic = true, bold = true },
		highlights = function(highlights)
			local C = require("default_theme.colors")

			highlights.DiagnosticError.bold = true
			highlights.DiagnosticHint.bold = true
			highlights.DiagnosticInfo.bold = true
			highlights.DiagnosticWarn.bold = true

			return highlights
		end,
		plugins = { -- enable or disable extra plugin highlighting
			aerial = false,
			beacon = true,
			bufferline = true,
			dashboard = true,
			highlighturl = true,
			hop = false,
			indent_blankline = true,
			lightspeed = false,
			["neo-tree"] = true,
			notify = false,
			["nvim-tree"] = false,
			["nvim-web-devicons"] = true,
			rainbow = true,
			symbols_outline = false,
			telescope = true,
			vimwiki = false,
			["which-key"] = true,

			cinnamon = false,
		},
	},

	-- Disable AstroNvim ui features
	ui = {
		nui_input = true,
		telescope_select = true,
	},

	-- Configure plugins
	plugins = {
		-- Add plugins, the packer syntax without the "use"
		toggleterm = {
			start_in_insert = true,
			insert_mappings = true, -- whether or not the open mapping applies in insert mode
			terminal_mappings = true,
			hide_numbers = true, -- hide the number column in toggleterm buffers
			close_on_exit = true, -- close the terminal window when the process exits
			shell = vim.o.shell, -- change the default shell
			on_config_done = nil,
		},

		["neo-tree"] = {
			window = {
				width = 40,

				mappings = {
					["<TAB>"] = "next_source",
					["<S-TAB>"] = "prev_source",
				},
			},

			default_component_configs = {
				indent = {
					with_markers = false,
				},
			},
		},
		telescope = {
			file_ignore_patterns = { "./node_modules/*", "node_modules", "^node_modules/*", "node_modules/*" },
			pickers = {
				current_buffer_fuzzy_find = {
					tiebreak = function(current_entry, existing_entry)
						return current_entry.lnum < existing_entry.lnum
					end,
				},
			},
			extensions = {
				recent_files = {
					only_cwd = true,
				},
			},
		},
		init = {
			-- You can disable default plugins as follows:
			-- ["goolord/alpha-nvim"] = { disable = true },

			-- You can also add new plugins here as well:
			-- { "andweeb/presence.nvim" },
			--
			--
			--

			-- {
			-- 	"ThePrimeagen/harpoon",
			-- 	config = function()
			-- 		require("harpoon").setup({
			-- 			menu = {
			-- 				width = 1000,
			-- 			},
			-- 		})
			-- 	end,
			-- },
			"rktjmp/lush.nvim",
			"kartikp10/noctis.nvim",
			{
				"anuvyklack/windows.nvim",
				requires = {
					"anuvyklack/middleclass",
					"anuvyklack/animation.nvim",
				},
				config = function()
					vim.o.winwidth = 10
					vim.o.winminwidth = 10
					vim.o.equalalways = false
					require("windows").setup()
				end,
			},
			{
				"zbirenbaum/copilot-cmp",
				module = "copilot_cmp",
				after = { "copilot.lua" },
				config = function()
					require("copilot_cmp").setup()
				end,
			},
			-- "github/copilot.vim",

			{
				"zbirenbaum/copilot.lua",
				event = { "VimEnter" },
				config = function()
					vim.defer_fn(function()
						-- require("copilot").setup()
						require("copilot").setup({
							cmp = {
								enabled = true,
							},
							panel = { -- no config options yet
								enabled = false,
							},
						})
					end, 100)
				end,
			},

			{
				"axelvc/template-string.nvim",
				config = function()
					require("template-string").setup({
						filetypes = {
							"typescript",
							"graphql",
							"svelte",
							"gql",
							"javascript",
							"typescriptreact",
							"javascriptreact",
						}, -- filetypes where the plugin is active
						jsx_brackets = true, -- should add brackets to jsx attributes
					})
				end,
			},

			{
				"olivercederborg/poimandres.nvim",
				config = function()
					require("poimandres").setup({
						bold_vert_split = true, -- use bold vertical separators
						dim_nc_background = false, -- dim 'non-current' window backgrounds
						disable_background = true, -- disable background
						disable_float_background = false, -- disable background for floats
						disable_italics = false, -- disable italics
					})
				end,
			},
			{ "jose-elias-alvarez/nvim-lsp-ts-utils" },
			{
				"smartpde/telescope-recent-files",
				config = function() end,
			},
			{ "shaunsingh/oxocarbon.nvim", run = "./install.sh" },
			{ "catppuccin/nvim", as = "catppuccin" },
			{
				"windwp/nvim-spectre",
				config = function()
					require("spectre").setup({

						color_devicons = true,
						open_cmd = "vnew",
						live_update = false, -- auto excute search again when you write any file in vim
						line_sep_start = "┌-----------------------------------------",
						result_padding = "¦  ",
						line_sep = "└-----------------------------------------",
						highlight = {
							ui = "String",
							search = "DiffChange",
							replace = "DiffDelete",
						},
						mapping = {
							["toggle_line"] = {
								map = "dd",
								cmd = "<cmd>lua require('spectre').toggle_line()<CR>",
								desc = "toggle current item",
							},
							["enter_file"] = {
								map = "<cr>",
								cmd = "<cmd>lua require('spectre.actions').select_entry()<CR>",
								desc = "goto current file",
							},
							["send_to_qf"] = {
								map = "<leader>q",
								cmd = "<cmd>quit<CR>",
								desc = "send all item to quickfix",
							},
							["replace_cmd"] = {
								map = "<leader>c",
								cmd = "<cmd>lua require('spectre.actions').replace_cmd()<CR>",
								desc = "input replace vim command",
							},
							["show_option_menu"] = {
								map = "<leader>o",
								cmd = "<cmd>lua require('spectre').show_options()<CR>",
								desc = "show option",
							},
							["run_current_replace"] = {
								map = "<leader>rc",
								cmd = "<cmd>lua require('spectre.actions').run_current_replace()<CR>",
								desc = "replace current line",
							},
							["run_replace"] = {
								map = "<leader>R",
								cmd = "<cmd>lua require('spectre.actions').run_replace()<CR>",
								desc = "replace all",
							},
							["change_view_mode"] = {
								map = "<leader>v",
								cmd = "<cmd>lua require('spectre').change_view()<CR>",
								desc = "change result view mode",
							},
							["change_replace_sed"] = {
								map = "trs",
								cmd = "<cmd>lua require('spectre').change_engine_replace('sed')<CR>",
								desc = "use sed to replace",
							},
							["change_replace_oxi"] = {
								map = "tro",
								cmd = "<cmd>lua require('spectre').change_engine_replace('oxi')<CR>",
								desc = "use oxi to replace",
							},
							["toggle_live_update"] = {
								map = "tu",
								cmd = "<cmd>lua require('spectre').toggle_live_update()<CR>",
								desc = "update change when vim write file.",
							},
							["toggle_ignore_case"] = {
								map = "ti",
								cmd = "<cmd>lua require('spectre').change_options('ignore-case')<CR>",
								desc = "toggle ignore case",
							},
							["toggle_ignore_hidden"] = {
								map = "th",
								cmd = "<cmd>lua require('spectre').change_options('hidden')<CR>",
								desc = "toggle search hidden",
							},
							-- you can put your mapping here it only use normal mode
						},
						find_engine = {
							-- rg is map with finder_cmd
							["rg"] = {
								cmd = "rg",
								-- default args
								args = {
									"--color=never",
									"--no-heading",
									"--with-filename",
									"--line-number",
									"--column",
								},
								options = {
									["ignore-case"] = {
										value = "--ignore-case",
										icon = "[I]",
										desc = "ignore case",
									},
									["hidden"] = {
										value = "--hidden",
										desc = "hidden file",
										icon = "[H]",
									},
									-- you can put any rg search option you want here it can toggle with
									-- show_option function
								},
							},
							["ag"] = {
								cmd = "ag",
								args = {
									"--vimgrep",
									"-s",
								},
								options = {
									["ignore-case"] = {
										value = "-i",
										icon = "[I]",
										desc = "ignore case",
									},
									["hidden"] = {
										value = "--hidden",
										desc = "hidden file",
										icon = "[H]",
									},
								},
							},
						},
						replace_engine = {
							["sed"] = {
								cmd = "sed",
								args = nil,
								options = {
									["ignore-case"] = {
										value = "--ignore-case",
										icon = "[I]",
										desc = "ignore case",
									},
								},
							},
							-- call rust code by nvim-oxi to replace
							["oxi"] = {
								cmd = "oxi",
								args = {},
								options = {
									["ignore-case"] = {
										value = "i",
										icon = "[I]",
										desc = "ignore case",
									},
								},
							},
						},
						default = {
							find = {
								--pick one of item in find_engine
								cmd = "rg",
								options = { "ignore-case" },
							},
							replace = {
								--pick one of item in replace_engine
								cmd = "sed",
							},
						},
						replace_vim_cmd = "cdo",
						is_open_target_win = true, --open file on opener window
						is_insert_mode = false, -- start open panel on is_insert_mode
					})
				end,
			},
			{
				"gbprod/yanky.nvim",
				event = "InsertEnter",
				config = function()
					require("yanky").setup({})
				end,
			},
			-- "jose-elias-alvarez/nvim-lsp-ts-utils",
			"EdenEast/nightfox.nvim",
			{
				"monaqa/dial.nvim",
			},
			{ "tpope/vim-repeat" },
			{
				"tpope/vim-surround",
				keys = { "c", "d", "y" },
			},
			"wellle/targets.vim",
			{
				"phaazon/hop.nvim",
				event = "BufRead",
				config = function()
					require("hop").setup()
				end,
			},
		},
		-- All other entries override the setup() call for default plugins
		["null-ls"] = function(config)
			local null_ls = require("null-ls")
			-- Check supported formatters and linters
			-- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/formatting
			-- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/diagnostics
			config.sources = {
				null_ls.builtins.formatting.stylua,
				null_ls.builtins.formatting.prettier.with({
					filetypes = {
						"javascript",
						"svelte",
						"typescript",
						"css",
						"scss",
						"html",
						"json",
						"yaml",
						"markdown",
						"graphql",
						"md",
						"txt",
					},
				}),
			}
			-- set up null-ls's on_attach function
			return config -- return final config table
		end,
		treesitter = {
			ensure_installed = { "lua", "svelte", "javascript", "css", "graphql" },
		},

		["mason-lspconfig"] = { -- overrides `require("mason-lspconfig").setup(...)`
			ensure_installed = { "tsserver", "svelte" },
		},
		-- use mason-null-ls to configure Formatters/Linter installation for null-ls sources
		["mason-null-ls"] = { -- overrides `require("mason-null-ls").setup(...)`
			-- ensure_installed = { "prettier", "stylua" },
		},
	},

	luasnip = {
		vscode = {
			-- Add paths for including more VS Code style snippets in luasnip
			paths = {

				"~/.config/astronvim/lua/user/snippets",
			},
		},
		vscode_snippet_paths = {
			-- "./lua/user/snippets",
			"~/.config/astronvim/lua/user/snippets",
			-- "../snippets",
		},
	},

	["which-key"] = {
		register_mappings = {
			n = {
				["<leader>"] = {

					["N"] = { "<cmd>tabnew<cr>", "New Buffer" },
				},
			},
		},
	},

	-- CMP Source Priorities
	-- modify here the priorities of default cmp sources
	-- higher value == higher priority
	-- The value can also be set to a boolean for disabling default sources:
	-- false == disabled
	-- true == 1000
	cmp = {

		source_priority = {

			copilot = 621,
			nvim_lua = 1000,
			nvim_lsp = 700,
			luasnip = 650,
			crates = 600,
			path = 500,
			buffer = 250,
		},
	},
	heirline = {
		-- -- Customize different separators between sections
		-- separators = {
		--   tab = { "", "" },
		-- },
		-- -- Customize colors for each element each element has a `_fg` and a `_bg`
		-- colors = function(colors)
		--   colors.git_branch_fg = astronvim.get_hlgroup "Conditional"
		--   return colors
		-- end,
		-- -- Customize attributes of highlighting in Heirline components
		-- attributes = {
		--   -- styling choices for each heirline element, check possible attributes with `:h attr-list`
		--   git_branch = { bold = true }, -- bold the git branch statusline component
		-- },
		-- -- Customize if icons should be highlighted
		-- icon_highlights = {
		--   breadcrumbs = false, -- LSP symbols in the breadcrumbs
		--   file_icon = {
		--     winbar = false, -- Filetype icon in the winbar inactive windows
		--     statusline = true, -- Filetype icon in the statusline
		--   },
		-- },
	},

	-- Extend LSP configuration
	lsp = {
		-- enable servers that you already have installed without lsp-installer
		servers = {
			-- "graphql",
		},

		formatting = {},

		-- override the lsp installer server-registration function
		server_registration = function(server, opts)
			require("lspconfig")[server].setup(opts)
		end,

		-- Add overrides for LSP server settings, the keys are the name of the server
		["server-settings"] = {
			-- example for addings schemas to yamlls
			--
			-- tsserver = {
			-- cmd = { "typescript-language-server", "--stdio", "--log-level", "3" },
			-- },
			svelte = {
				plugin = {
					css = {
						enable = false,
						diagnostics = {
							enable = false,
						},
					},
				},
				settings = {
					svelte = {
						plugin = {
							css = {
								enable = false,
								validate = false,
								diagnostics = {
									enable = false,
								},
							},
							svelte = {

								compilerWarnings = {
									["missing-declaration"] = "ignore", -- <<< This doesn't work, svelte still spams me with this warning
									["svelte3/ignore-styles"] = "ignore", -- <<< This doesn't work, svelte still spams me with this warning
									["css-unused-selector"] = "ignore", -- <<< This doesn't work, svelte still spams me with this warning
									["css-syntax-error"] = "ignore", -- <<< This doesn't work, svelte still spams me with this warning
								},

								css = {
									enable = false,
									validate = false,
									diagnostics = {
										enable = false,
									},
								},
							},
						},
					},
				},
			},
			--
			-- yamlls = {
			--   settings = {
			--     yaml = {
			--       schemas = {
			--         ["http://json.schemastore.org/github-workflow"] = ".github/workflows/*.{yml,yaml}",
			--         ["http://json.schemastore.org/github-action"] = ".github/action.{yml,yaml}",
			--         ["http://json.schemastore.org/ansible-stable-2.9"] = "roles/tasks/*.{yml,yaml}",
			--       },
			--     },
			--   },
			-- },
		},
	},

	-- Diagnostics configuration (for vim.diagnostics.config({}))
	diagnostics = {
		virtual_text = true,
		virtual_lines = false,
		underline = true,
	},

	-- This function is run last
	-- good place to configure mappings and vim options
	polish = function()
		-- Set key bindings
		vim.keymap.set("n", "<C-s>", ":w!<CR>")

		local augend = require("dial.augend")
		require("dial.config").augends:register_group({
			default = {
				augend.integer.alias.decimal,
				augend.integer.alias.hex,
				augend.date.alias["%Y/%m/%d"],
			},
			typescript = {
				augend.integer.alias.decimal,
				augend.integer.alias.hex,
				augend.constant.alias.bool,
				augend.constant.alias.bool,
				augend.date.alias["%Y/%m/%d"],
				augend.constant.new({
					elements = { "&&", "||" },
					word = false,
					cyclic = true,
				}),
				augend.constant.new({
					elements = { "!=", "==" },
					word = false,
					cyclic = true,
				}),
				augend.constant.new({ elements = { "let", "const" } }),
			},
			visual = {
				augend.integer.alias.decimal,
				augend.integer.alias.hex,
				augend.date.alias["%Y/%m/%d"],
				augend.constant.alias.alpha,
				augend.constant.alias.Alpha,
			},
		})

		local map = vim.api.nvim_set_keymap
		local unmap = vim.api.nvim_del_keymap
		unmap("n", "<leader>pc")
		unmap("n", "<leader>pi")
		unmap("n", "<leader>ps")
		unmap("n", "<leader>pS")
		unmap("n", "<leader>pu")

		unmap("n", "<C-q>")

		-- unmap("t", "<esc>")
		-- unmap("t", "jk")

		unmap("t", "<C-h>")
		unmap("t", "<C-j>")
		unmap("t", "<C-k>")
		unmap("t", "<C-l>")
		-- DIAL
		map("n", "<C-a>", require("dial.map").inc_normal("typescript"), { noremap = true })
		map("n", "<C-x>", require("dial.map").dec_normal("typescript"), { noremap = true })

		-- YANKY
		map("n", "p", "<Plug>(YankyPutAfter)", {})
		map("n", "P", "<Plug>(YankyPutBefore)", {})
		map("x", "p", "<Plug>(YankyPutAfter)", {})
		map("x", "P", "<Plug>(YankyPutBefore)", {})
		map("n", "gp", "<Plug>(YankyGPutAfter)", {})
		map("n", "gP", "<Plug>(YankyGPutBefore)", {})
		map("x", "gp", "<Plug>(YankyGPutAfter)", {})
		map("x", "gP", "<Plug>(YankyGPutBefore)", {})

		-- Diagnostic
		map("n", "ge", "<cmd>lua vim.diagnostic.open_float()<CR>", {})

		map("n", "<c-n>", "<Plug>(YankyCycleForward)", {})
		map("n", "<c-p>", "<Plug>(YankyCycleBackward)", {})

		map("n", "y", "<Plug>(YankyYank)", {})
		map("x", "y", "<Plug>(YankyYank)", {})

		-- Spectre
		--
		map("n", "<leader>rr", "<cmd>lua require('spectre').open()<CR>", {})
		map("n", "<leader>rc", "<cmd>lua require('spectre').open_visual({select_word=true})<CR>", {})
		map("n", "<leader>rf", "<cmd>lua require('spectre').open_file_search()<CR>", {})
		map("n", "<leader>rs", "<cmd>lua require('spectre').open_file_search({select_word=true})<CR>", {})

		-- AERIAL
		-- map("n", "<leader>a", "<cmd>AerialToggle!<CR>", {})
		-- map("n", "<Up>", "<cmd>AerialPrev<CR>", {})
		-- map("n", "<Down>", "<cmd>AerialNext<CR>", {})

		-- require("telescope").load_extension("aerial")
		-- HOP
		map("n", "s", ":HopWord<cr>", { silent = true })
		map("n", "S", ":HopWord<cr>", { silent = true })

		map("n", "<leader>lS", ":Telescope lsp_workspace_symbols<cr>", { desc = "Find workspace symbols" })
		map("n", "<leader>lR", ":LspRestart<cr>", { desc = "Restart" })
		map("n", "<leader>sp", ":SessionManager load_session<cr>", { desc = "Projects" })
		-- Set autocommands
		-- vim.api.nvim_create_augroup("packer_conf", { clear = true })
		-- vim.api.nvim_create_autocmd("BufWritePost", {
		-- 	desc = "Sync packer after modifying plugins.lua",
		-- 	group = "packer_conf",
		-- 	pattern = "plugins.lua",
		-- 	command = "source <afile> | PackerSync",
		-- })

		local Terminal = require("toggleterm.terminal").Terminal
		local lazygit = Terminal:new({ cmd = "lazygit", hidden = false, direction = "tab" })
		local pter = Terminal:new({ cmd = "pter ~/todo.txt", hidden = true, direction = "tab" })

		---@diagnostic disable-next-line: lowercase-global
		function _lazygit_toggle()
			lazygit:toggle()
		end

		---@diagnostic disable-next-line: lowercase-global
		function _pter_toggle()
			pter:toggle()
		end

		map("n", "<leader>gg", "<cmd>lua _lazygit_toggle()<CR>", { noremap = true, silent = true })
		map("n", "<leader>gt", "<cmd>lua _pter_toggle()<CR>", { noremap = true, silent = true })
		map("n", "<leader>Q", "<cmd>qa!<CR>", { noremap = true, silent = true })
		map("n", "<leader>W", "<cmd>wa<CR>", { noremap = true, silent = true })

		-- map("n", "<leader>gt", "<cmd>lua _pter_toggle()<CR>", { noremap = true, silent = true })
		-- map("n", "<TAB>", "<cmd>cn<CR>", { noremap = true, silent = true })
		-- map("n", "<S-TAB>", "<cmd>cp<CR>", { noremap = true, silent = true })

		map("n", "<leader>ao", "<cmd>TSLspOrganize<cr>", { noremap = true, desc = "Organize Imports", silent = true })
		map("n", "<leader>aa", "<cmd>TSLspImportAll<cr>", { noremap = true, desc = "Import All", silent = true })
		map("n", "<leader>ar", "<cmd>TSLspRenameFile<cr>", { noremap = true, desc = "Rename File", silent = true })

		astronvim.add_user_cmp_source({
			name = "copilot",
		})

		vim.cmd("set cmdheight=2")

		vim.g.catppuccin_flavour = "mocha"
		require("catppuccin").setup()

		-- vim.cmd([[colorscheme noctis]])
	end,
}

function hasQFOpened()
	local buffers = vim.api.nvim_list_bufs()
	local res = false

	for _, buffer in ipairs(buffers) do
		if vim.api.nvim_buf_is_loaded(buffer) then
			local filename = vim.api.nvim_buf_get_option(buffer, "filetype")

			if filename == "qf" and vim.fn.getbufinfo(buffer)[1].hidden == 0 then
				res = true
			end
		end
	end

	return res
end

return config
